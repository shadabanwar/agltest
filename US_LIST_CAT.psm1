# This is the module 
if(!$global:HOME_DIR) {
	$global:HOME_DIR =  $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath("$PSScriptRoot")
}


function US_List_CAT
{
[CmdletBinding()]
Param(
    [Parameter(Position=0, Mandatory=$true)] [string]$JsonUrl                    
)

Write-Host ($JsonUrl | Out-String) -ForegroundColor green
$result=0

try{
        $Url = $JsonUrl
        $oWc = New-Object System.Net.WebClient
        $webpage = $oWc.DownloadData($Url) 
        $content = [System.Text.Encoding]::ASCII.GetString($webpage)

        $Json = ConvertFrom-Json -InputObject $content
        Write-Host "Male:"
        $Json | Where-Object { $_.gender -eq "Male" } | ForEach-Object{
            $_.pets | Sort-Object {$_.name} | Where-Object {$_.type -eq "Cat"} | foreach {
            Write-Host "   ",$_.name
            }           
        }
        Write-Host "Female:"
        $Json | Where-Object { $_.gender -eq "Female" } | ForEach-Object{
        $_.pets | Sort-Object {$_.name} | Where-Object {$_.type -eq "Cat"} | foreach {
        Write-Host "   ",$_.name
            }           
        }     
}
catch {
            $errorMsg = getExceptionMessage
            LogException "US_List_Cat Failed $errorMsg"
            $result = 1
}
return $result 

}


function US_List_CAT_Gender
{
[CmdletBinding()]
Param(
    [Parameter(Position=0, Mandatory=$true)] [string]$JsonUrl,    
    [Parameter(Position=1, Mandatory=$true)] [string]$Gender    
          
)

Write-Host ($JsonUrl | Out-String) -ForegroundColor green
$result=0

try{
        $Url = $JsonUrl
        $oWc = New-Object System.Net.WebClient
        $webpage = $oWc.DownloadData($Url) 
        $content = [System.Text.Encoding]::ASCII.GetString($webpage)

        $Json = ConvertFrom-Json -InputObject $content
        Write-Host $Gender
        $Json | Where-Object { $_.gender -eq $Gender } | ForEach-Object{
            $_.pets | Sort-Object {$_.name} | Where-Object {$_.type -eq "Cat"} | foreach {
            Write-Host "   ",$_.name
            }           
        }
       
}
catch {
            $errorMsg = getExceptionMessage
            LogException "US_List_Cat Failed $errorMsg"
            $result = 1
}
return $result 

}


function getExceptionMessage() {
    $exceptionDetails = $_.Exception.ToString()
    $scriptName = $MyInvocation.ScriptName
    $scriptLine = $MyInvocation.ScriptLineNumber
    $invocationScriptLine = $_.InvocationInfo.ScriptLineNumber
    $invocationOffsetLine = $_.InvocationInfo.OffsetInLine
    $functionName =  (Get-Variable MyInvocation -Scope 1).Value.MyCommand.Name
    $positionMessage = $_.InvocationInfo.PositionMessage
    $errorMsg = "Caught an Exception in Script [$scriptName] and Function [$functionName] at line [$scriptLine] : $exceptionDetails [line=$invocationScriptLine; offset=$invocationOffsetLine] $positionMessage"
    return $errorMsg
}
Export-ModuleMember -function US_List_CAT
Export-ModuleMember -function US_List_CAT_Gender