## Execute ps1 file


1. Download **US_LIST_CAT.psm1** to your prefer directory.
2. Download **ListCats.ps1** to your prefer directory.
3. Edit **ListCats.ps1** to include the path for the module file **US_LIST_CAT.psm1**.
4. The statement **US_List_CAT_Gender -JsonUrl $JsonUrl ** in the ps1 file runned and promplte will be displayed to ask to enter gender
	1. Female
	     Garfield
		 Tabby
		 Simba
    2. Male
		Garfield
		Jim
		Max
		Tom

## Run - WebAPI

   1- Clone the repository to your local machine.
   2- Open AglCodingTest.sln.
   3- Build the solution.
   5- Press F5 key to build and run the instance locally.
   6- Open a web browser and type https://<domain>:port/api/Pets to run the application, or use postman, fiddler, soapui, jmeter to execute the get method.
   7- The required result will be displayed.
