﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLCodingTest.Models
{
    public enum PersonGenderType
    {
       
        Unknown = 0,

      
        Male = 1,

        
        Female = 2
    }
}