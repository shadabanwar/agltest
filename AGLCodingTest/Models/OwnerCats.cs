﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLCodingTest.Models
{
    public class OwnerCats
    {
        public IEnumerable<string> Male { get; set; }

        public IEnumerable<string> Female { get; set; }
    }
}