﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLCodingTest.Models
{
    public enum PetType
    {
        Unknown = 0,
        Cat = 1,
        Dog = 2,
        Fish = 3
    }
}