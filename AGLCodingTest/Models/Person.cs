﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLCodingTest.Models
{
    public class Person
    {
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="Models.GenderType"/> value.
        /// </summary>
       
        public PersonGenderType Gender { get; set; }

        /// <summary>
        /// Gets or sets the age.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets the list of <see cref="Pet"/> objects.
        /// </summary>
        public List<Pets> Pets { get; set; }
    }
}