﻿using AGLCodingTest.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace AGLCodingTest.Controllers
{
    public class PetsController : ApiController
    {
        [HttpGet]
        public async Task<OwnerCats> Get()
        {
            List<Person> PetOwner = new List<Person>();
            OwnerCats catsresponse = new OwnerCats();

            try
            {
                var httpHandler = new HttpClientHandler { UseDefaultCredentials = true };
                using (var client = new HttpClient(httpHandler))
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["AGLAzureSite"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("");

                    response.EnsureSuccessStatusCode();
                    PetOwner = await response.Content.ReadAsAsync<List<Person>>();
                }

                catsresponse.Male = SortFunctionHelper(PetOwner, PersonGenderType.Male);
                catsresponse.Female = SortFunctionHelper(PetOwner, PersonGenderType.Female);

                
            }
            catch (Exception ex)
            {

                throw new HttpResponseException(HttpStatusCode.InternalServerError);

            }
                        
            return catsresponse;
        }

        private IEnumerable<string> SortFunctionHelper(List<Person> petOwner, PersonGenderType personGender)
        {
            var response = (from owner in petOwner
                            where owner.Pets != null && owner.Gender == personGender
                            from pet in owner.Pets
                            where pet.type == PetType.Cat
                            orderby pet.name
                            select pet.name).ToList();

            return response;

        }

    }
}
